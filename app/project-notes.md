---
title: "Project Notes"
output: html_document
---

# Project notes

Total time on project to date: 30 hours

## How to edit
 
* From the `www/` folder in the `app/` folder, you can edit:
  * The titlebar name of the app (`info.txt`)
  * the text on the Welcome (`intro.md`) and About tabs (`about.md`), and the instructions/explanations on the Weights  (`weights.md`) and Results (`results.md`) tabs by editing the Markdown documents in the `text/` folder.
  * Change the logo (shown in top right corner, currently the NIHR logo), by saving your logo of choice as `logo.png`
  * Update the underlying data, by saving it using the defined names in the `data/` subfolder  
<br>
* Please don't edit the following in the `www/` folder:
  * `app.js`
  * `criteriaplot.png`

## Notes

* I created a copy of the `mdmcda` package on a private GitHub repo, as shinyapps.io doesn't support packages hosted on GitLab. In order to deploy, you'll need to install the package from GitHub or CRAN.
<br>
* For the "Define scenarios" tab:
  * The "Add scenario" button brings up modal to capture user input. Text-based responses are converted to numbers and input to the table. __For this to work, it relies on the options in column 4 ("choices") of `scenarios.xlsx` being seperated with a comma/ an ampersand ("&").__ A modal approach was taken, as trying to align the selectInputs with the appropriate row of the table was impractical, particularly when the user resizes the window. 
  * Only user added scenarios can be deleted using the "Remove last scenario" - the base scenarios are protected.
  * Data validation has been implemented to provide feedback if users try and edit a cell to a value that it not allowed.
* The criteria plot has been designed to be rendered once

## Errors

None (whoop!)


