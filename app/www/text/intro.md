---
title: "info"
output: html_document
---

# MDMA Policy Think Tank

This app accompanies the results of the MDMA Policy Think Tank that are available through https://mdmapolicy.com/results and the Open Science Framework repository at https://osf.io/h58r6/.

The think tank process was developed as a way to use the drug policy expertise of a group of scientists and other experts while minimizing the potential for bias.

## The objective phase: expert input

The first three steps in the process are to establish:

- the outcomes ("criteria" in MCDA terms, such as prevalence of MDMA use; health damage; MDMA-related organised crime; or damage to the enviroenment);
- the policy instruments ('decisions' in MCDA terms, such as "legal status of MDMA possession"; "license required for MDMA sale to consumers", "minimum age"); and
- and the options that comprise each decison ('alternatives' in MCDA terms, such as, for "license required for MDMA sale to consumers": "No license required", "License required", and "Not applicable").

The next step is to estimate the effects of all alternatives within all decisions (all policy options within all instruments) on all criteria (outcomes). For example, if selling MDMA would require a license, how would that impact the prevalence of MDMA use, health damage, MDMA-related organised crime, and damage to the environment?

After all these effects have been estimated, that concludes the more objective expert input part of the exercise.

## The subjective phase: scenarios and weights

The next two steps are mostly informed by personal or political preference and so are more subjective. These two steps are specifying the following two things.

- The policy models: the scenarios that each consist of one selected option (alternative) for each policy instrument (decision). For example, under a prohibitive policy, MDMA sales to consumers would be illegal, so no minimum age would be defined; whereas under a regulatory policy, the minimum age can be set to 18 years.
- The weights for the outcomes (criteria). For example, somebody might value user health very much, but international politics much lower; and somebody else might value crime reduction very highly, and economical outcomes very low.

Once these have been specified, it is possible to weigh all the estimates using the specified weights, and then compute the total scores for all scenarios given the selected alternatives that comprise them.

# What this app does and how to use it

This app allows you to specify your own policy model(s) and to decide how you want to weight the outcomes.

To use the app, click the <span style="background-color: #272727; color: #fff; 13px; font-weight: 300; padding: 3px 5px; margin: -3px 5px;">Analysis</span> tab at the tip. The "Analysis" section opens at the "Define scenarios" tab, where you can add or remove scenarios (policy models).

You can also change the weights (the default settings are the weights used by the think tank). To do this, click the "Specify weights" tab. Note that one of the weights always has to be at 100; you specify the weights of the other criteria (outcomes) relative to this most important criterion. The criteria tree at the right-hand side shows the current weights of each criterion.

Once you are satisfied with your settings, open the <span style="background-color: #272727; color: #fff; 13px; font-weight: 300; padding: 3px 5px; margin: -3px 5px;">Results</span> tab at the top. The app will then start computing how each scenario you specified will score. You'll see a comparison of all scenarios' scores, and you can inspect how every every scenario's total score is made up exactly.

