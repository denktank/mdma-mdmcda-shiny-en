## About

### The project

This tab will contain more details. For now, these mostly consist of links to pages that hold those details.

### The think tank experts

This section will list the think tank members.

### Contact

If you have questions, you can contact us through mdma@mdmabeleid.nl.

### FAQ

#### **Where can I learn more?** If you want to learn more about the background, the preprint we will soon publish online will be the best place to start. If you want to see (many) more results, check out the GitLab repository at https://mdmabeleid.nl/results or the Open Science Framework repository at https://osf.io/h58r6/.

#### **Where can I find the original data and analysis scripts?** These are available in our GitLab repository at https://mdmabeleid.nl/results and the Open Science Framework repository at https://osf.io/h58r6/.


