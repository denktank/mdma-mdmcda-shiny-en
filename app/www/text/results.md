## Results

This tab shows the results of the scenarios (policy models) you defined and the weights you set in the "Analysis" tab. The main figure shows each scenario's performance on the seven criteria clusters, and the table to the right shows the total score. Both are sorted from highest-scoring to lowest-scoring. This overview contains, in addition to the scenarios specified in the "Analysis" tab, two "reference scenarios" that have been automatically generated: the highest-scoring scenario and the lowest scoring scenario.

You can explore the scenarios more in detail using the tabs below, to see which decisions (policy instruments) and which criteria (outcomes) contribute most to the final scores.
