## How to weigh results

The think tank defined 27 criteria (outcomes of a specific policy), that are organised into 7 criteria clusters (groups of outcomes). Below, you can indicate how important you find each cluster, and within each cluster, you can finetune the importance of the criteria that make up that cluster.

To do this, first decide that you think is the most important cluster (or within a cluster, the most important criterion), and set its weight to 100. Then, indicate how important you think the other clusters (or criteria) are by setting their weight relative to the import important one.

Each criterion's weight is multiplied by the weight of its cluster to arrive at its final weight. The weights of the clusters and criteria, as well as the final weight, are shown in the criteria tree to the right.
