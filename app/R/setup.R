#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
# Import data from files in the "data/" folder

###-----------------------------------------------------------------------------
### Variables that may vary from project to project
###-----------------------------------------------------------------------------

estimate_column <- 'meanWeights_weighted_estimate';
weight_columns <- c(raw = 'weight_mean_proportion',
                    rescaled = 'weight_mean_rescaled_proportion');
### Set variable names
scorerId <- "all";
weightProfileName <- "meanWeights"
weightedEstimateName <- paste0(weightProfileName, "_weighted_estimate");
#weightedEstimateName <- paste0(weightProfileName, "_weighed_estimate");
tempWeightedEstimateName <- paste0(scorerId, "_", weightedEstimateName);

###-----------------------------------------------------------------------------
### Potentially override mdmcda defaults for column names
###-----------------------------------------------------------------------------

criterionId_col            <- mdmcda::opts$get("criterionId_col");
criterionLabel_col         <- mdmcda::opts$get("criterionLabel_col");
parentCriterionId_col      <- mdmcda::opts$get("parentCriterionId_col");
decisionId_col             <- mdmcda::opts$get("decisionId_col");
decisionLabel_col          <- mdmcda::opts$get("decisionLabel_col");
decisionDescription_col    <- mdmcda::opts$get("decisionLabel_col");
alternativeValue_col       <- mdmcda::opts$get("alternativeValue_col");
alternativeLabel_col       <- mdmcda::opts$get("alternativeLabel_col");
scenarioId_col             <- mdmcda::opts$get("scenarioId_col");
scenarioLabel_col          <- mdmcda::opts$get("scenarioLabel_col");
weightProfileId_col        <- mdmcda::opts$get("weightProfileId_col");
leafCriterion_col          <- mdmcda::opts$get("leafCriterion_col");
rootCriterionId            <- mdmcda::opts$get("rootCriterionId");
score_col                  <- mdmcda::opts$get("score_col");

###-----------------------------------------------------------------------------
### Paths and filenames
###-----------------------------------------------------------------------------

### Path with data files
wwwDir <- file.path("www", "data");
### For testing
#wwwDir <- file.path("app", wwwDir);

### Prefix for all files
filePrefix <- "mdmcda-data--";

### Convenience function to create the filename
filename <- function(x) file.path(wwwDir, paste0(filePrefix, x));

### Filenames to read for the data and specifications
criteriaFile           <- filename("fullCriteriaDf.xlsx");
estimatesFile          <- filename("full-estimates-spreadsheet.xlsx");
weightsFile            <- filename("weightsMeansAndSDs.xlsx");
scenariosFile          <- filename("defined-scenario-definitions.xlsx");
scenariosInColumnsFile <- filename("scenario-definitions.xlsx");

### Filenames to read for the human-readable labels
criterionLabelsFile    <- filename("criterionLabels.xlsx");
decisionLabelsFile     <- filename("decisionLabels.xlsx");
scenarioLabelsFile     <- filename("scenarioLabels.xlsx");
alternativeLabelsFile  <- filename("alternativeLabels.xlsx");

###-----------------------------------------------------------------------------
### Read data and specifications
###-----------------------------------------------------------------------------

scenarios_obsoleteFormat <-
  mdmcda::read_scenarioDefinitions_in_columns_from_xl(scenariosInColumnsFile);

scenarioDefinitions <-
  mdmcda::read_scenarioDefinitions_from_xl(scenariosFile);

criteria <- mdmcda::read_criteria_from_xl(criteriaFile,
                                showGraphs = FALSE)

estimates <-
  mdmcda::read_estimates_from_xl(estimatesFile);

weights <-
  mdmcda::read_weightsMeansAndSDs_from_xl(weightsFile);

###-----------------------------------------------------------------------------
### Read labels and descriptions
###-----------------------------------------------------------------------------

criterionLabels <-
  mdmcda::read_criterionLabels_from_xl(criterionLabelsFile);
decisionLabels <-
  mdmcda::read_decisionLabels_from_xl(decisionLabelsFile);
decisionDescriptions <-
  mdmcda::read_decisionDescriptions_from_xl(decisionLabelsFile);
scenarioLabels <-
  mdmcda::read_scenarioLabels_from_xl(scenarioLabelsFile);
alternativeLabels <-
  mdmcda::read_alternativeLabels_from_xl(alternativeLabelsFile);

###-----------------------------------------------------------------------------
### Set orders for criteria, decisions, and scenarios
###-----------------------------------------------------------------------------

criterionOrder <-
  setdiff(names(criterionLabels),
          criteria$convenience$parentCriterionIds);
parentCriterionOrder <-
  intersect(names(criterionLabels),
            criteria$convenience$topCriterionClusters);

decisionOrder <- names(decisionLabels);
scenarioOrder <- names(scenarioLabels);

### Note that this scenario order vector now contains all scenarios in the
### original MDMCDA project. So we set it to just the predefined scenarios:
scenarioOrder <- intersect(scenarioOrder, names(scenarioDefinitions));

###-----------------------------------------------------------------------------
### This concludes the basic loading and setting of stuff. Ideally, most
### stuff below here can be removed by using different names in the app
### and by consistently using the label vectors, indexed by the order vectors
### or a subset of those where applicable, and the other objects.
### Note that all columns should be indexed using the names set above;
### if I missed something, please let me know and I'll add it :-)
###-----------------------------------------------------------------------------




### Reproduce the 'scenarios' object, based on the now obsolete file format
### read by `mdmcda::read_scenarioDefinitions_in_columns_from_xl`

scenarios <- data.frame(
  decision_id = decisionOrder,
  decision_label = decisionLabels[decisionOrder],
  decision_description = decisionDescriptions[decisionOrder]
);

scenarios$decision_alternatives <-
  unlist(
    lapply(
      1:length(alternativeLabels[decisionOrder]),
      function(j) {
        return(
          mdmcda::vecTxt(
            paste0(
              unlist(alternativeLabels[decisionOrder][[j]]), " (",
              names(alternativeLabels[decisionOrder][[j]]), ")"
            )
          )
        );
      }
    )
  );

scenarios <-
  cbind(scenarios,
        lapply(scenarioDefinitions,
               function(x) return(x[decisionOrder])))

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
# Pre-processing

### Order weights so that they match the order of the criteria
weights_ordered <-
  weights[order(match(weights[, criterionId_col],
                      criteria$criteriaDf[, criterionId_col])), ]
weights_ordered <-
  weights_ordered[c(1:length(weights[, criterionId_col])), ]

###-----------------------------------------------------------------------------
###-----------------------------------------------------------------------------
###
### Note for Luke: note that criterionOrder is the 'authoritative order' for
### the criteria. I assume that means it should be used for the lines just above
### this comment, but I'm not sure. Could you check? :-)
###
### Note that criterionOrder only contains the leafs; to also get their parents,
### we also need parentCriterionOrder.
###
###
### Also see the lines directly below; I commented out and replaced to get
### stuff from the 'default objects'.
###
###-----------------------------------------------------------------------------
###-----------------------------------------------------------------------------

### Get number of scenario options (/decisions)
nrows_scenario <- length(decisionOrder);
#nrows_scenario <- dim(scenarios)[1]

### Get number of predefined scenarios
### This limits what the user can delete from the table
n_predefined_scenarios <- length(scenarioDefinitions);
#n_predefined_scenarios <- ncol(scenarios)

### Get number of criteria
nrows_criteria <- length(criterionOrder);
#nrows_criteria <- dim(criteria$criteriaDf)[1]

### Get name of top-level criteria
top_level_name <- rootCriterionId;
  #criteria$criteriaDf$criterion_id[which(criteria$criteriaDf$parentCriterion == "-")]

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
# Generate "criteria" convience object

###-----------------------------------------------------------------------------
###
### Note for Luke: This is now already in the criteria object :-)
###
### Here, I set the objects with the new names.
###
###-----------------------------------------------------------------------------

criterionIds <- criteria$convenience$criterionIds;
parentCriterionIds <- criteria$convenience$parentCriterionIds_by_childId;
parentCriteriaIds <- criteria$convenience$parentCriterionIds;
childCriteriaIds <- criteria$convenience$childCriterionIds_by_parentId;
childCriteriaByCluster <- criterionOrder;

###-----------------------------------------------------------------------------
###
### I don't dare, but - could you do a CTRL-SHIFT-F find/replace for this?
###
### Ideally, the app uses the objects already provided; so basically replace:
###
### criteria$convenience$childCriteriaByCluster  with  criterionOrder
### criteria$convenience$parentCriterionIds      with  criteria$convenience$parentCriterionIds_by_childId
### criteria$convenience$parentCriteriaIds       with  criteria$convenience$parentCriterionIds
### criteria$convenience$childCriteriaIds        with  criteria$convenience$childCriterionIds_by_parentId
### criteria$convenience$criterionIds            -- can stay the same :-)
###
###-----------------------------------------------------------------------------

# criterionIds <-
#   sort(unique(criteria$criteriaDf$id))
#
# parentCriterionIds <-
#   as.data.frame(unique(criteria$criteriaDf[, c('id',
#                                                'parentCriterion')]))
#
# parentCriterionIds <-
#   stats::setNames(parentCriterionIds$parentCriterion,
#                   parentCriterionIds$id)
#
# parentCriteriaIds <-
#   unique(criteria$criteriaDf[criteria$criteriaDf$parentCriterion == "outcomes",
#                              'id'])
#
# parentCriteriaIds <-
#   unique(parentCriterionIds)[nchar(unique(parentCriterionIds)) > 1]
#
#
# childCriteriaIds <-
#   stats::setNames(lapply(unique(parentCriterionIds),
#                          function(i)
#                            names(parentCriterionIds[parentCriterionIds ==
#                                                       i])),
#                   unique(parentCriterionIds))
#
#
# childCriteriaByCluster <-
#   criteria$criteriaDf[criteria$criteriaDf[, leafCriterion_col], ]
# childCriteriaByCluster <-
#   childCriteriaByCluster[, criterionId_col][
#     order(childCriteriaByCluster[, parentCriterionId_col])
#   ];
#
#
# criteria$convenience <-
#   list(
#     criterionIds = criterionIds,
#     parentCriterionIds = parentCriterionIds,
#     parentCriteriaIds = parentCriteriaIds,
#     childCriteriaIds = childCriteriaIds,
#     childCriteriaByCluster = childCriteriaByCluster
#   )

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
# Import the labels for the plots

# Set scenario labels
# NB: Order of scenarios in the file is important! Defines order in plots

static_scenarioLabels <- scenarioLabels;

###-----------------------------------------------------------------------------
### This is already provided now at the top, including the order
###-----------------------------------------------------------------------------

# scenarioLabels_df <-
#   openxlsx::read.xlsx("app/www/data/mdmcda-data--scenarioLabels.xlsx")  %>%
#   dplyr::mutate(dplyr::across(dplyr::everything(), stringr::str_trim))
#
# static_scenarioLabels <- scenarioLabels_df[, 2]
# names(static_scenarioLabels) <- scenarioLabels_df[, 1]

# Set criteria lables

###-----------------------------------------------------------------------------
### This is now also provided already at the top, including the order
###-----------------------------------------------------------------------------

criteriaLabels <- criterionLabels;
criteriaOrder <- criterionOrder;

# criteriaLabels_df <-
#   openxlsx::read.xlsx("www/data/data_mdma-criteria-labels_en.xlsx")  %>%
#   mutate(across(everything(), stringr::str_trim)) %>%
#   filter(id %in% criteria$criteriaDf$id) %>%
#   filter(!(id %in% parentCriteriaIds))
#
# criteriaLabels <- criteriaLabels_df[, 2]
# names(criteriaLabels) <- criteriaLabels_df[, 1]
#
# criteriaOrder <- names(criteriaLabels)

# Set parentCriterion labels

###-----------------------------------------------------------------------------
### These are now in the same "criterionLabels" object, since elements are
### selected by index anyway, and we have
### criteria$convenience$parentCriterionIds, so
### criterionLabels[criteria$convenience$parentCriterionIds] gives us to
### criterion labels. The order is also already stored in
### parentCriterionOrder (see above).
###-----------------------------------------------------------------------------

parentCriterionLabels <-
  criterionLabels[criteria$convenience$parentCriterionIds];

# parentCriterionLabels_df <-
#   openxlsx::read.xlsx("www/data/mdmcda-data--parentCriteriaLabels.xlsx") %>%
#   mutate(across(everything(), stringr::str_trim))
#
# parentCriterionLabels <- parentCriterionLabels_df[, 2]
# names(parentCriterionLabels) <- parentCriterionLabels_df[, 1]
#
# parentCriterionOrder <-
#   stringr::str_trim(names(parentCriterionLabels))

# Set decision labels

###-----------------------------------------------------------------------------
### Same story - already set at the top, including the order
###-----------------------------------------------------------------------------

# decisionLabels_df <-
#   openxlsx::read.xlsx("www/data/mdmcda-data--decisionLabels_nl.xlsx")  %>%
#   mutate(across(everything(), stringr::str_trim))
#
# decisionLabels <- decisionLabels_df[, 2]
# names(decisionLabels) <- decisionLabels_df[, 1]
#
# decisionOrder <- names(decisionLabels)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
# Generate criteria graph

# Wrap labels greater than 20 characters to make the plot look a bit better!
criteriaLabelsPlot <-
  gsub('(.{1,20})(\\s|$)', '\\1\\\\n', criterionLabels)

### We'll need this later, too, for the new 'flagship plot'.
### Interesting - I never knew you could also wrap using regular
### expressions! I'll have to analyse that one, I don't quite understand
### what it does yet :-) I always use this bit:
wrappedScenarioLabels <-
  unlist(lapply(scenarioLabels, function(x)
    paste(strwrap(x, 10), collapse="\n")
  ))

###-----------------------------------------------------------------------------
###
### If the file is used to display the graph anyway, why render it - why not set
### renderGraph = FALSE? Should speed things up a bit, no?
###
### Also, I've used the new functionality of mdmcda::plot_criteria to directly
### export the plot.
###
###-----------------------------------------------------------------------------

# ## Add weights to criteria object (note: data.tree Node objects are R6, so
# ## passed by reference instead of value; hence, we don't store the invisibly
# ## returned object)
# mdmcda::add_weights_to_criteriaTree(
#   weightsMeansAndSDs = weights,
#   criteria = criteria,
#   weightCols = weight_columns
# );
#
# ### After this call, "weight_mean_rescaled_proportion" is stored in every node
# ### in the tree as 'rescaled', and 'rescaled_product' is added which holds
# ### the final weight for every leaf node.
# ###
# ### This command does the same but also updates the `weightsMeansAndSDs` that
# ### here is called `weights`:
#
# # weights <-
# #   mdmcda::combine_weights_and_criteria(
# #     weights,
# #     criteria,
# #     weightCols = weight_columns
# #   );
#
# ### Note also that the weight shown in the criteria plot isn't the weight used
# ### for multiplication: that is weights$rescaled_total_percentage (which is
# ### created by the call to `mdmcda::combine_weights_and_criteria`).
#
# # Export graph to PNG
# mdmcda::plot_criteria(
#   criteria,
#   labels = criteriaLabelsPlot,
#   renderGraph = FALSE,
#   returnSVG = FALSE,
#   outputFile = file.path("app","www", "criteriaplot.png")
# )
