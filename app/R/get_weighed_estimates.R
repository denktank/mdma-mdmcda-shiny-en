get_weighed_estimates <- function(scenarios, estimates, weightsMeansAndSDs, criteria, scenarioOrder, scenarioLabels) {


###-----------------------------------------------------------------------------
### Aggregate weight estimates
###-----------------------------------------------------------------------------

# weightsMeansAndSDs <- mdmcda::weightsMeansAndSDs(weights);

###-----------------------------------------------------------------------------
### Set the weight of cultural values (liberal vs conservative) to 0 for the
### rest of the computations (because the think tank has no political/idealistic
### preference).
###-----------------------------------------------------------------------------

# This will be need tryCatch for adjustable underlying data -

  # Change to 0 in dataset

# weightsMeansAndSDs[weightsMeansAndSDs$criterion_id == "cultural_values",
#                    c("weight_mean_proportion",
#                      "weight_mean_rescaled_proportion")] <-
#   0;

###-----------------------------------------------------------------------------
### Combine with criteria tree to accumulate over the hierachy
###-----------------------------------------------------------------------------

combinedWeightsAndCriteria <-
  mdmcda::combine_weights_and_criteria(weightsMeansAndSDs,
                                       criteria);

###-----------------------------------------------------------------------------
### Extract and set complemented objects
###-----------------------------------------------------------------------------

weightsMeansAndSDs <- combinedWeightsAndCriteria$weightsMeansAndSDs;
criteria <- combinedWeightsAndCriteria$criteria;

###-----------------------------------------------------------------------------
### Weight plots
###-----------------------------------------------------------------------------

###-----------------------------------------------------------------------------
### Compile weight profile
###-----------------------------------------------------------------------------

weightProfiles <-
  mdmcda::create_weight_profile(weightsMeansAndSDs = weightsMeansAndSDs,
                               criteria = criteria,
                               profileName = "meanWeights");
weightProfileNames <- names(weightProfiles);


## Final weights


weightsMeansAndSDs$isCluster <-
  weightsMeansAndSDs$criterion_id %in% weightsMeansAndSDs$parentCriterion_id;


### Cluster weights


tmpDf <- weightsMeansAndSDs[weightsMeansAndSDs$isCluster, ];

tmpDf[order(tmpDf$rescaled_total, decreasing = TRUE),
                   c('criterion_id', 'rescaled_product')]
rm(tmpDf);


### Criteria weights


tmpDf <- weightsMeansAndSDs[!weightsMeansAndSDs$isCluster, ];

knitr::kable(tmpDf[order(tmpDf$rescaled_total, decreasing = TRUE),
                   c('criterion_id', 'rescaled_total')], row.names=FALSE);
rm(tmpDf);


###-----------------------------------------------------------------------------
### Compute scores per scenario
###-----------------------------------------------------------------------------

### Some preparation
scenarioNames <-
  setdiff(names(scenarios),
          c("id", "label", "description", "choices"));
scenarioDefinitions <-
  lapply(scenarioNames, function(scenarioName) {
    return(stats::setNames(scenarios[, scenarioName],
                           scenarios$id));
  });
names(scenarioDefinitions) <- scenarioNames;
decisionNames <- scenarios$id;
criterionNames <- names(criteriaLabels);

sink(tempfile())
### Create dataframe for the weighed estimates
weighedEstimates <-
  mdmcda::build_weighted_estimate_df(multiEstimateDf = estimates$multiEstimateDf,
                                    criterionNames = criterionNames,
                                    decisionNames = decisionNames,
                                    scenarioNames = scenarioNames,
                                    scenarioDefinitions = scenarioDefinitions,
                                    scorer = "all",
                                    setMissingEstimates=0);
sink()

### Actually weigh the estimates
weighedEstimates <-
  mdmcda::weight_estimates_by_profile(weighted_estimate_df = weighedEstimates,
                                      weight_profiles = weightProfiles);

### Add weights and weighed estimates to multiEstimateDf
for (currentWeightProfile in weightProfileNames) {

  estimates$multiEstimateDf[[paste0(currentWeightProfile, "_weight")]] <-
    weightProfiles[[currentWeightProfile]][estimates$multiEstimateDf$criterion_id];

  estimates$multiEstimateDf[[paste0(currentWeightProfile,
                                    "_weighted_estimate")]] <-
#                                    "_weighed_estimate")]] <-
    estimates$multiEstimateDf$all *
    estimates$multiEstimateDf[[paste0(currentWeightProfile,
                                      "_weight")]];

}

###-----------------------------------------------------------------------------
### Performance tables
###-----------------------------------------------------------------------------

performanceTables <-
  lapply(
    unique(weighedEstimates$scenario_id),
    function(currentScenario) {
      return(tidyr::pivot_wider(
        weighedEstimates[weighedEstimates$scenario_id==currentScenario,
                         c("decision_id",
                           "criterion_id",
                           "meanWeights_weighted_estimate")],
                           #"meanWeights_weighed_estimate")],
        id_cols="decision_id",
        names_from="criterion_id",
        values_from="meanWeights_weighted_estimate"
        #values_from="meanWeights_weighed_estimate"
      ));
    }
  );
names(performanceTables) <- unique(weightedEstimates$scenario_id);
#names(performanceTables) <- unique(weighedEstimates$scenario_id);

###-----------------------------------------------------------------------------
### Best scenario
###-----------------------------------------------------------------------------

### Do the computations
sink(tempfile())
scores_per_alternative <-
  mdmcda::compute_scores_per_alternative(
    multiEstimateDf = estimates$multiEstimateDf,
    weightProfiles = weightProfiles
  )

sink()

bestAlternatives <-
  mdmcda::compute_best_alternatives(
    scores_per_alternative=scores_per_alternative,
    ignoreRegex = "^0$"
  );
worstAlternatives <-
  mdmcda::compute_worst_alternatives(
    scores_per_alternative=scores_per_alternative,
    ignoreRegex = "^0$"
  );

###-----------------------------------------------------------------------------
### Add scenario with best scores to list of scenarios
###-----------------------------------------------------------------------------

bestScenario <-
  as.numeric(gsub("^.*\\s(\\d+)$",
                  "\\1",
                  bestAlternatives$alternative_id));
names(bestScenario) <- bestAlternatives$decision_id;

worstScenario <-
  as.numeric(gsub("^.*\\s(\\d+)$",
                  "\\1",
                  worstAlternatives$alternative_id));
names(worstScenario) <- worstAlternatives$decision_id;

scenarioDefinitions <-
  c(scenarioDefinitions,
    list(optimal_scenario = bestScenario,
         worst_scenario = worstScenario));

###-----------------------------------------------------------------------------
### Add tweaked scenario to list of scenarios
###-----------------------------------------------------------------------------

xShopScenario <-
  bestScenario;

xShopTweaks <-
  c('posession_status' = 3,
    'advertising' = 1,
    'consumer_sale_legal_status' = 4);

xShopScenario[names(xShopTweaks)] <-
  xShopTweaks;

scenarioDefinitions <-
  c(scenarioDefinitions,
    list(x_shop = xShopScenario));

###-----------------------------------------------------------------------------
### Generate weighted estimate dataframe again with these two new scenarios
###-----------------------------------------------------------------------------

sink(tempfile())
### Create dataframe for the weighed estimates
weighedEstimates <-
  mdmcda::build_weighted_estimate_df(multiEstimateDf = estimates$multiEstimateDf,
                                     criterionNames = criterionNames,
                                     decisionNames = decisionNames,
                                     scenarioNames = names(scenarioDefinitions),
                                     scenarioDefinitions = scenarioDefinitions,
                                     scorer = "all",
                                     setMissingEstimates=0);

sink()

### Actually weigh the estimates
weighedEstimates <-
  mdmcda::weight_estimates_by_profile(weighted_estimate_df = weighedEstimates,
                                      weight_profiles = weightProfiles);

### Add parent criterion identifiers
weighedEstimates$parentCriterion_id <-
  parentCriterionIds[as.character(weighedEstimates$criterion_id)];

#### Overview

###-----------------------------------------------------------------------------
### Total scores per scenario
###-----------------------------------------------------------------------------


return(list(weighedEstimates, weightProfileNames))
}
